## Rscript to generate summary statistics and plot coverage metrics for sequencing data
## Author: kjwade
## Date: 5/13/20
## Input [1] runID/barcode [2] coverage_histogram.txt [3] coverage_consecutive_intervals.txt [4] coverage_nucleotde.txt
## called by: sh minion_covid_coverage.sh

args = commandArgs(trailingOnly=TRUE)

runID=args[1]
covhist <- read.delim(args[2], header=FALSE)
consecutiveInts <- read.delim(args[3], header=FALSE)
nucleotide <- read.delim(args[4], header=FALSE)

# Make boxplot of per nucleotide coverage distribution
bxplt=paste(runID,'coverage_nucleotide_boxplot.PNG',sep='_')
png(bxplt)
boxplot(nucleotide$V3, main=paste(runID,'- per nucleotide coverage',sep=' '),xlab='Nucleotides',ylab='Coverage depth')
dev.off()

# Generate boxplot stats
bxpltstats=boxplot.stats(nucleotide$V3)
nuc0=subset(nucleotide, nucleotide$V3==0)
summaryfile=paste(runID,'coverage_nucleotide_summary.txt',sep='_')
write.table("Boxplot stats, coverage depth: [1] min [2] 1st quartile [3] median [4] 3rd quartile [5] upper whisker", file = summaryfile, row.names=F, col.names=F)
write.table(bxpltstats$stats, file=summaryfile, append=T, col.names=F)
write.table("Genome size (bp):", file = summaryfile, append = T, col.names=F, row.names=F)
write.table(bxpltstats$n, file = summaryfile, append = T, col.names=F, row.names=F)
write.table("Coverage depth confidence interval about the median:", file = summaryfile, append = T, row.names=F, col.names=F)
write.table(bxpltstats$conf, file = summaryfile, append = T, col.names=F, row.names=F)
write.table("Mean coverage depth:", file = summaryfile, append = T, col.names=F, row.names=F)
write.table(mean(nucleotide$V3), file=summaryfile, append = T, col.names=F, row.names=F)
write.table("Maximum single nucleotide coverage depth:", file = summaryfile, append = T, col.names=F, row.names=F)
write.table(max(nucleotide$V3), file = summaryfile, append = T, col.names=F, row.names=F)
write.table("Number of bases with zero coverage:", file = summaryfile, append = T, col.names=F, row.names=F)
write.table(length(nuc0$V3), file = summaryfile, append = T, col.names=F, row.names=F)


# Calculate and plot cumulative coverage curve
genome=subset(covhist,covhist$V1=='genome')
genome[,'cumcov']=cumsum(genome$V3)
genome[,'percentAscend']=genome$cumcov/length(nucleotide$V3)

cumcov=paste(runID,'cumulative_genome_coverage.PNG',sep='_')
png(cumcov,width=820, height=480)
plot(genome$percentAscend~genome$V2,type="l",lwd=3,ylab="Percent of ref genome covered",xlab='Coverage depth', main= paste(runID,'- Cumulative genome coverage',sep=' '))
dev.off()

#Plot fraction of bases at each coverage depth
fracCov=paste(runID,'fraction_bases_coverage.PNG',sep='_')
png(fracCov, width=820, height=480)
plot(covhist$V5~covhist$V2,pch=16,ylab="Fraction of bases with given coverage depth",xlab='Coverage depth', main= paste(runID,'- Fraction of total bases per coverage depth',sep=' '))
dev.off()

#Plot consecutive coverage across genome
consCov=paste(runID,'consecutive_coverage_on_genome.PNG',sep='_')
png(consCov, width=820, height=480)
plot(nucleotide$V3~nucleotide$V2,pch=16,ylab="Coverage depth",xlab='Genomic position', main= paste(runID,'- Coverage depth across the genome',sep=' '))
lines(nucleotide$V3~nucleotide$V2, lwd=2)
dev.off()




