## Driver script to align, variant call, QC and create consensus genome from a single minion barcode at a time
## 8/27/21
## kjwade
## bcftools version: 1.11
## samtools version: 1.10

# Args:[1] <barcodeRunIDString>
sh alignReads.sh $1

# [2] 
sh minionCovidCoverage.sh 

# [3]
sh genConsensusGenome.sh

