## Script to generate a consensus covid genome that has been masked for low quality
## 8/21/21
## kjwade

for vcf in *sorted.vcf.gz;
do
fname="$(echo $vcf|cut -d '.' -f 1,1)"
## Index:
bcftools index $vcf
echo $fname
echo 'SNPs indexed'

## Make consensus:
cat NC_045512.2.fasta| bcftools consensus $vcf > $fname'_consensus.fasta'

echo 'consensus made'

## Identify low quality (<30X) genome regions--> .bed file
python findLowQualBases.py $fname'_coverage_nucleotide.txt'

## Mask new genome using .bed file from previous step
bedtools maskfasta -fi $fname'_consensus.fasta' -bed $fname'_coverage_nucleotide_lowQual.bed' -fo $fname'_consensus_masked.fasta'

echo 'consensus masked'
done;
