#!/bin/bash
## Script to pipeline the generation of coverage data for minion reads
## Author: kjwade
## Date: 5/13/20
## Updated: 8/31/20
## Input: $1 - batch#

## Args:
# $1 - fastq containing directory (./covidSeqsBatch1/barcode01/)
# $2 - barcode ID (01, 02,etc.)
# $3 - batch ID (1, 2, 3, etc.)


for bam in *_sorted.bam; 
do
runID="$(echo $bam | cut -d '.' -f 1)"

# Generate histogram of coverage values
bedtools genomecov -ibam $bam > $runID'_coverage_histogram.txt' 

# Generate coverage across consecutive interval ranges
bedtools genomecov -ibam $bam -bga > $runID'_coverage_consecutive_intervals.txt'

# Generate coverage at each individual nucleotide
bedtools genomecov -ibam $bam -d > $runID'_coverage_nucleotide.txt'

# Run Rscript to generate summary stats, plots of coverage metrics
Rscript minionCovidCoverage.R $runID $runID'_coverage_histogram.txt' $runID'_coverage_consecutive_intervals.txt' $runID'_coverage_nucleotide.txt'


done;

