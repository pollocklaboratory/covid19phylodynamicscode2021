## Script to evaluate the single nucleotide coverage of the sequence output, identify those >= 30X and write those positions to bedfile
## 12/7/20
## kjwade
## [1] *_coverage_nucleotide.txt
import sys
def main():
	cov=open(sys.argv[1])
	name=sys.argv[1].split('.')[0]
	name=str(name+'_lowQual.bed')
	out=open(name,'w')
	for i in cov:
		i=i.strip('\n').split('\t')
		if int(i[2].rstrip())<=30:
			#print i[2]
			out.write('%s\t%s\t%s\n'%(i[0],i[1],i[1]))
	out.close()
	cov.close()

main()
