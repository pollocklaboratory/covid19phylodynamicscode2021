
Custom scripts to process minion nanopare long read genome sequencing, by sequencing barcode
author: kjwade
date: 21 August 2021


[1] For each barcode: Wrapper to align fastq reads, generate variant calls relative to reference (NC_045512.2.fasta), identify and mask low quality regions (coverage <30X) and create a new consensus sequence

### Driver script #########################################
Args:
$1 - barcodeRunIDString

USAGE in ./example/:
sh runCovidSeqs.sh FAO08355_pass_barcode08_9b2bcfbb
###########################################################



### Sub-scripts executed within driver script################
[1] Wrapper to align reads and call variants relative to reference
USAGE:
sh alignReads.sh <barcodeRunIDString>

[2] Wrapper to generate sequencing quality summary stats
USAGE:
sh minionCovidCoverage.sh

[3] Wrapper to generate consensus sequence with mutations from reference genome
s
USAGE:
sh genConsensusGenome.sh
##############################################################
